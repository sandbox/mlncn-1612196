<?php

/**
 * @file
 * Order view and edit pages for making new and changing libcat book orders.
 */

/**
 * Title callback for order pages.
 */
function libcat_order_page_title($order) {
  return $order->title;
}
