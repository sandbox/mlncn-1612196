<?php
/**
 * @file
 * Administration pages for configuring and managing libcat book orders.
 */

/**
 * Module configuration options.
 */
function libcat_order_config_form($form, &$form_state) {
  $form = array();
  $form['libcat_order_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Book order recipient e-mail address.'),
    '#default_value' => variable_get('libcat_order_email', ''),
    '#size' => 40,
    '#maxlength' => 255,
    '#description' => t('Book order recipient e-mail address - where all requests will be sent.'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Menu callback: manage library catalog order requests.
 */
function libcat_order_manage_filter_form($form, $form_state) {
  $form['filter'] = libcat_order_filter_form();
  $form['#submit'][] = 'libcat_order_filter_form_submit';
  $form['admin'] = libcat_order_manage_form();

  return $form;
}

/**
 * Form builder: Builds the order administration overview.
 *
 * Modeled on node_admin_nodes().
 */
function libcat_order_manage_form() {
  $admin_access = user_access(LIBCAT_ADMIN_ADMINISTER_CATALOG);

  // Build the Bulk operations / Update options form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk operations'),
    '#attributes' => array('class' => array('container-inline')),
    '#access' => $admin_access,
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => libcat_order_manage_form_operations(),
    '#default_value' => LIBCAT_ORDER_STATUS_ORDERED,
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#validate' => array('libcat_order_manage_form_validate'),
    '#submit' => array('libcat_order_manage_form_submit'),
  );

  // Build the sortable table header.
  $header = array(
    'title' => array('data' => t('Title'), 'field' => 'o.title'),
    'copies' => array('data' => t('Copies'), 'field' => 'o.copies'),
    'collection_id' => t('Ordered by'),
    'status' => array('data' => t('Status'), 'field' => 'o.status'),
    'updated' => array('data' => t('Updated date'), 'field' => 'o.updated', 'sort' => 'desc')
  );
  $header['operations'] = array('data' => t('Operations'));

  $query = db_select('libcat_order', 'o')->extend('PagerDefault')->extend('TableSort');
  libcat_order_build_filter_query($query);

  // @TODO by default restrict the query to requests.
  // $query->condition('o.status', 1);
  $orders = $query
    ->fields('o', array('order_id', 'collection_id', 'asin', 'title', 'copies', 'instructions', 'status', 'note', 'placed', 'updated'))
    ->limit(50)
    ->orderByHeader($header)
    ->execute()
    ->fetchAll();

  // Prepare the list of orders.
  $destination = drupal_get_destination();
  $options = array();
  foreach ($orders as $order) {
    $options[$order->order_id] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $order->title,
          '#href' => 'http://www.amazon.com/gp/product/' . $order->asin,
          '#options' => array('attributes' => array('title' => 'See this book on Amazon')),
        ),
      ),
      'copies' => $order->copies,
      'collection_id' => libcat_collection_load($order->collection_id)->name,
      'status' => libcat_order_status_display($order->status),
      'updated' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => format_date($order->updated, 'short'),
          '#href' => 'libcat/order/' . $order->order_id,
          '#options' => array('attributes' => array('title' => 'See and edit this order')),
        ),
      ),
    );
    // Build a list of all the accessible operations for the current order.
    $operations = array();
    // Reusing module_invoke_all('libcat_order_operations'); does not fly here.
    $available_operations = array(
      LIBCAT_ORDER_STATUS_ORDERED => array(
        'label' => t('place'),
        'tooltip' => t('Place an order for this book at Amazon.com.'),
      ),
      LIBCAT_ORDER_STATUS_CANCELED => array(
        'label' => t('cancel'),
        'tooltip' => t('Reject and cancel this order (for reasons other than the book not being sourceable).'),
      ),
      LIBCAT_ORDER_STATUS_COMPLETED => array(
        'label' => t('complete'),
        'tooltip' => t('Complete this order: book entries will be created.'),
      ),
      LIBCAT_ORDER_STATUS_NOT_SOURCEABLE => array(
        'label' => t('void'),
        'tooltip' => t('Mark this order not sourceable.'),
      ),
    );
    // Do not provide a link to change to the status the order already has.
    unset($available_operations[$order->status]);
    foreach ($available_operations as $key => $op) {
      $query = array(
        'status' => $key,
      ) + $destination;
      $operations[$key] = array(
        'title' => $op['label'],
        'href' => 'libcat/order/' . $order->order_id,
        'query' => $query,
        'attributes' => array('title' => $op['tooltip']),
      );
    }
    $options[$order->order_id]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $options[$order->order_id]['operations'] = array(
        'data' => array(
          '#theme' => 'links__libcat_order_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $options[$order->order_id]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }
  }

  $empty_text = t('No orders.');

  // Only use a tableselect when the current user is able to perform any
  // operations.  Otherwise, use a simple table.
  if ($admin_access) {
    $form['orders'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => $empty_text,
    );
  }
  else {
    $form['orders'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => $empty_text,
    );
  }
  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Validate libcat_order_manage_form form submissions.
 *
 * Check if any orders have been selected to perform the chosen operation.
 */
function libcat_order_manage_form_validate($form, &$form_state) {
  // Error if there are no items to select.
  if (!is_array($form_state['values']['orders']) || !count(array_filter($form_state['values']['orders']))) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Process libcat_order_manage_form form submissions.
 *
 * Execute the chosen operation on the selected orders.
 */
function libcat_order_manage_form_submit($form, &$form_state) {
  $operations = module_invoke_all('libcat_order_operations');
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked orders.
  $orders = array_filter($form_state['values']['orders']);
  $function = $operation['callback'];
  if ($function) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($orders), $operation['callback arguments']);
    }
    else {
      $args = array($orders);
    }
    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step.  This may never be
    // necessary as we do not have a confirmation step.
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Operation options for library catalog manage orders form.
 */
function libcat_order_manage_form_operations() {
  $options = array();
  foreach (module_invoke_all('libcat_order_operations') as $operation => $opinfo) {
    $options[$operation] = $opinfo['label'];
  }
  return $options;
}

/**
 * Return form for order administration filters.
 */
function libcat_order_filter_form() {
  $session = isset($_SESSION['libcat_order_filter']) ? $_SESSION['libcat_order_filter'] : array();
  $filters = libcat_order_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'exposed_filters__libcat_order',
  );
  $multiple_filters = count($session) > 1 ? TRUE : FALSE;
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    $value = $filters[$type]['options'][$value];
    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('where %property is %value', $t_args));
    }
    // Prevent trying to filter twice on option that can have only one setting.
    if (in_array($type, array('status', 'type'))) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  $form['filters']['status'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('clearfix')),
    '#prefix' => (($multiple_filters && $i) ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  );
  $form['filters']['status']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => $filter['title'],
      '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => count($session) ? t('Refine') : t('Filter'),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['status']['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_js('misc/form.js');

  return $form;
}

/**
 * Process result from libcat orders administration filter form.
 */
function libcat_order_filter_form_submit($form, &$form_state) {
  $filters = libcat_order_filters();
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Flatten the options array to accommodate hierarchical/nested options.
          $flat_options = form_options_flatten($filters[$filter]['options']);
          // Only accept valid selections offered on the dropdown, block bad input.
          if (isset($flat_options[$form_state['values'][$filter]])) {
            $_SESSION['libcat_order_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['libcat_order_filter']);
      break;
    case t('Reset'):
      $_SESSION['libcat_order_filter'] = array();
      break;
  }
}

/**
 * Implements hook_libcat_order_operations().
 */
function libcat_order_libcat_order_operations() {
  $operations = array(
    'order' => array(
      'label' => t('Place order for selected book requests'),
      'callback' => 'libcat_order_mass_update',
      'callback arguments' => array('updates' => array('status' => LIBCAT_ORDER_STATUS_ORDERED)),
    ),
    'complete' => array(
      'label' => t('Complete selected orders; book nodes will be created'),
      'callback' => 'libcat_order_mass_update',
      'callback arguments' => array('updates' => array('status' => LIBCAT_ORDER_STATUS_COMPLETED)),
    ),
    'cancel' => array(
      'label' => t('Cancel selected orders'),
      'callback' => 'libcat_order_mass_update',
      'callback arguments' => array('updates' => array('status' => LIBCAT_ORDER_STATUS_CANCELED)),
    ),
    'not_sourceable' => array(
      'label' => t('Mark selected orders not sourceable'),
      'callback' => 'libcat_order_mass_update',
      'callback arguments' => array('updates' => array('status' => LIBCAT_ORDER_STATUS_NOT_SOURCEABLE)),
    ),
  );
  return $operations;
}

/**
 * List order administration filters that can be applied.
 */
function libcat_order_filters() {
  $filters['status'] = array();

  // Filter by status.  We may also want to filter by collection, but that
  // should be made a dropdown that responds via AJAX to the status selected and
  // only shows applicable collections (this is especially important as the
  // number of closed (completed, canceled, not sourceable) orders grows.
  $filters['status'] = array(
    'title' => t('status'),
    'options' => array(
      '[any]' => t('any'),
    ) + libcat_order_statuses(),
  );

  return $filters;
}

/**
 * Apply filters for library catalog order management based on session.
 *
 * @param $query
 *   A SelectQuery to which the filters should be applied.
 */
function libcat_order_build_filter_query(SelectQueryInterface $query) {
  // Build query
  $filter_data = isset($_SESSION['libcat_order_filter']) ? $_SESSION['libcat_order_filter'] : array();
  foreach ($filter_data as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'status':
        $query->condition('o.' . $key, $value);
        break;
    }
  }
}
