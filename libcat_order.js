// Enter ISBN number and have the rest of the book info filled in on the form
//
// Same approach and base functionality as libcat_book/addbooks.js.
//

(function($, Drupal) {
  Drupal.behaviors.libcatOrderAdd = {
    attach: function (context, settings) {
      $('#edit-asin', context).blur(function() {
        var asin = $('#edit-asin').val();
        // Initialize the form fields
        $('#edit-title').val('');
        // Get Amazon book info based on ISBN lookup
        $.get(settings.libcatUtilAmazonQuery, {
          addBook: 1,
          asin: asin
        },
        function(xml) {
          $(xml).find('Book').each(function() {
            var title = $(this).find('Title').text();
            var amazonlink = $(this).find('AmazonLink').text();

            $('#edit-title').val(title);
            $('#edit-amazonlink').val(amazonlink);
          });
        });
      })
    }
  }
})(jQuery, Drupal);

